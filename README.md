# README #

Enable the umass_global_branding module. There is no configuration. See umass_global_branding.module and files for location of external static resources.

This module pulls in the static assets from this repository: https://bitbucket.org/nsssystems/qs-global-umass-brand-assets/src/master/
That have been added to this public URL: https://www.umass.edu/static/branding/